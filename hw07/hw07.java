//Weiyao Shi
//wes319
//HW07
//Word tools
import java.util.Scanner; 
public class hw07{ 
  public static void main(String[] args){ //main method for menu
    Scanner sc= new Scanner(System.in);
    System.out.println("Please enter a paragraph");
    String sentence = sc.nextLine();
    System.out.println("You entered: "+sentence);
    System.out.println("c - Number of non-whitespace characters"); 
    System.out.println("w - Number of words"); 
    System.out.println("f - Find text"); 
    System.out.println("r - Replace all !'s"); 
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println("Choose an option:"); 
    char menuChar = printMenu();
    
    switch(menuChar)//choice of which method to use
    { 
       
      case 'c':
       System.out.println("Number of non-whitespace characters "+numChar(sentence));  
      break; 
      case 'w':
        
        System.out.println("Number of words: " + getNumOfWords(sentence));
      break;
      case'f': 
        
         System.out.print("instances: "+findText(sentence));
      break;
      case'r' :
        System.out.println("Edited text: "+replaceE(sentence));
      break;
      case's':
        System.out.println("Edited text: "+replaceS(sentence));
      break;
      case'q': 
        System.out.print("Quit"); 
      System.exit(0);
    } 
     
  } 
  public static char printMenu(){//method for choosing options from menu
    Scanner sc3 = new Scanner(System.in);
    char choice = sc3.nextLine().charAt(0);
    return choice;
  }
  public static int getNumOfWords(String sentence) { //method for counting words     
    String[]words = sentence.split(" ");
    //System.out.println("Word count is " + wordCount);
    return words.length; 
  } 
   public static int numChar(String sentence) { //method for counting characters
    sentence = sentence.trim().replaceAll("\\s","");
   
    //System.out.println("Number of character is "+charCount);
    return sentence.length();
  } 
  public static int findText(String sentence) { //method for counting occurence of text
    System.out.print("Enter a word or phrase to be found:"); 
    Scanner sc2 =  new Scanner(System.in);
    String findWord = sc2.nextLine();
    System.out.println(findWord);
    int textCount = 0;
    int i = 0;
    while((i = sentence.indexOf(findWord))!=-1){
      sentence = sentence.substring(i+findWord.length());
      textCount +=1;
    }
    //System.out.println("Number of character is "+charCount);
    return textCount; 
  } 
  public static String replaceE(String sentence){//method for replacing exclamations
    String exclamation="!";
    sentence = sentence.replace(exclamation,".");
    //System.out.println(sentence);
    return sentence;
  }
  public static String replaceS(String sentence){//method for replacing Spaces
    sentence = sentence.replaceAll(" ","");
    return sentence;
  }
} 
 
 
 
 
 
 
 

