//Weiyao Shi wes319
//cse002 lab3
//9/14
//Check


import java.util.Scanner;//import statement
public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {
          Scanner myScanner = new Scanner( System.in );//scanner for the code
          System.out.print("Enter the original cost of the check in the form xx.xx: ");
          double checkCost = myScanner.nextDouble();//scanner for original cost
          System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
          double tipPercent = myScanner.nextDouble();//scanner for percentage tip
          tipPercent /= 100; //convert the percentage into a decimal value
          System.out.print("Enter the number of people who went out to dinner: ");
          int numPeople = myScanner.nextInt();//scanner for number of people
          double totalCost;
          double costPerPerson;
          int dollars,   //dollar amount should be whole number
          dimes, pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
          totalCost = checkCost * (1 + tipPercent);//calculate total cost
          costPerPerson = totalCost / numPeople;
          dollars=(int)costPerPerson;//get whole amount
          //get dimes amount, e.g., 
// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
//  where the % (mod) operator returns the remainder
//  after the division:   583%100 -> 83, 27%5 -> 2 
          dimes=(int)(costPerPerson * 10) % 10;
          pennies=(int)(costPerPerson * 100) % 10;
          System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);//print out the results

          
         

}  //end of main method   
  	} //end of class
