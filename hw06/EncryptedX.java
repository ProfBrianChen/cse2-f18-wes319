//Weiyao Shi
//wes319
//HW06 Nested loop

import java.util.Scanner;
public class EncryptedX{     
 public static void main(String args[]) {
 
  Scanner sn = new Scanner(System.in);
  System.out.print("Please enter number of Rows between 1 and 100: ");
  
int nRows = sn.nextInt();//Scan integer
while(nRows < 0||nRows > 100){//Check if input is correct
 System.out.print("Invalid integer input");
 nRows = new Scanner(System.in).nextInt();
}
   for(int i = 0; i < nRows; i++ ) {
    for(int j = 0; j < nRows; j++) {
      if(i == j||j == (nRows - (i+1))){//Print Space to form X
        System.out.print(" ");
      }
      else{
        System.out.print("*");//Print * outside of x
      }
    }
    
    System.out.println();

}
 
 }
 }
  
