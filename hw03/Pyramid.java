//Weiyao Shi wes319
//cse002 hw03
//Pyramid
import java.util.Scanner;//import statement
public class Pyramid{
    			// main method required for every Java program
   			public static void main(String[] args) {
          Scanner myScanner = new Scanner( System.in );//define scanner
          System.out.print("The square side of the pyramid is (input length): ");
          double length = myScanner.nextDouble();//scanner for square side of pyramid
          System.out.print("The height of the pyramid is (input height): ");
          double height= myScanner.nextDouble();//scanner for height of pyramid
          double volume = length*length*height/3;//calculate pyramid volume
          System.out.println( "The volume inside the pyramid is: "+ volume);//print out result
          
        }
}
