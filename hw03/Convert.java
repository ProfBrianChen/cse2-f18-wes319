//Weiyao Shi wes319
//HW03 program 1
//Convert
import java.util.Scanner;//import statement
public class Convert{
    			// main method required for every Java program
   			public static void main(String[] args) {
          Scanner myScanner = new Scanner( System.in );//define scanner
          System.out.print("Enter the affected area in acres: ");
          double affectedArea = myScanner.nextDouble();//scanner for affected area
          System.out.print("Enter the rainfall in the affected area: ");
          double rainfall= myScanner.nextDouble();//scanner for rainfall
          double avgDrop = (rainfall*1.57828e-5)*(affectedArea*0.0015625);//calculate average rainfall and unit conversion
          System.out.println( avgDrop + " cubic miles");//print out result
          
        }
}
