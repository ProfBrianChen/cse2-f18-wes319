//Lab 2 Arithmetic Calculations 9/7
//CSE002
//Weiyao Shi wes319
public class Cyclometer{
   public static void main(String args[]){
     //input data
  int secsTrip1=480;  //time spent for trip1
  int secsTrip2=3220;  //time spent for trip2
	int countsTrip1=1561;  //number of counts for trip1
	int countsTrip2=9037; //number of counts for trip2
    // Constants
  double wheelDiameter=27.0,  //diameter of wheels in 
  PI=3.14159, //
  feetPerMile=5280,  //conversion
  inchesPerFoot=12,   //conversion
  secondsPerMinute=60;  //conversion
	double distanceTrip1, distanceTrip2,totalDistance;  // define variables
    
     
  System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");//print outcomes of trip1 and trip 2
	System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");  
     
  distanceTrip1 = countsTrip1*wheelDiameter*PI;//the distance of trip1 in inches
	distanceTrip1 = inchesPerFoot*feetPerMile; //convert into miles
	distanceTrip2 = countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//distans of trip2 in inches
	totalDistance = distanceTrip1+distanceTrip2;//convert into miles

     
  //print output
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");

  
  
  
  
   }//end of main method
  
}//end of class