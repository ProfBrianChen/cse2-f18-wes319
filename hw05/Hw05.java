//Weiyao Shi
//wes319
//HW05 While loop

import java.util.Scanner;
import java.text.DecimalFormat;

import java.util.Random;//generate random number
public class Hw05 {     
  public static void main(String[] a) {   
    System.out.println("Put in the number of hands:");
    Scanner sc = new Scanner(System.in);//define scanner
     int numHand = sc.nextInt();//ask for number of hands to generate
     System.out.println("The number of hands is "+ numHand);
     final int NUMBER_OF_CARDS = 52;//maximum number of cards
    int numRun = 0;//initialize counters
    int numAce = 0;
    int numJack = 0;
    int numQueen = 0;
    int numKing = 0;
    int num9 = 0;
    int num8 = 0;
    int num7 = 0;
    int num6 = 0;
    int num5 = 0;
    int num4 = 0;
    int num3 = 0;
    int num2 = 0;
    int four = 0;
    int three = 0;
    int one = 0;
     while( numRun < numHand ) {//loop for generate several hands
        int number = (int)(Math.random() * NUMBER_OF_CARDS);//generate random card
       // System.out.println(number);
       // System.out.print("The card you picked is ");
        if (number % 13 == 0){//determine number on cards
          numAce = numAce + 1;
         // System.out.print("Ace of ");
        }
        else if (number % 13 == 10){
          numJack = numJack+1;
          //System.out.print("Jack of ");
        }
        else if (number % 13 == 11){
          numQueen = numQueen + 1;
         // System.out.print("Queen of ");
        }
        else if (number % 13 == 12){
          numKing = numKing + 1;
         // System.out.print("King of ");
        }
        else if (number % 13 == 9){
          num9 = num9 + 1;
          //System.out.print((number % 13) + " of ");
        }
        else if (number % 13 == 8){
          num8 = num8 + 1;
         // System.out.print((number % 13) + " of ");
        }
        else if (number % 13 == 7){
          num7 = num7 + 1;
         // System.out.print((number % 13) + " of ");
        }
        else if (number % 13 == 6){
          num6 = num6 + 1;
         // System.out.print((number % 13) + " of ");
        }
        else if (number % 13 == 5){
          num5 = num5 + 1;
         // System.out.print((number % 13) + " of ");
        }
        else if (number % 13 == 4){
          num4 = num4 + 1;
         // System.out.print((number % 13) + " of ");
        }
        else if (number % 13 == 3){
          num3 = num3 + 1;
        //  System.out.print((number % 13) + " of ");
        }
        else if (number % 13 == 2){
          num2 = num2 + 1;
        //  System.out.print((number % 13) + " of ");
        }
        else{
         // System.out.print("none");
        }
        if (number / 13 == 0){//determine suits
         // System.out.println("Clubs");
        }
        else if (number / 13 == 1){
         // System.out.println("Diamonds");
        }
        else if (number / 13 == 2){
         // System.out.println("Hearts");
        }
        else if (number / 13 == 3){
       //   System.out.println("Spades");
        }
        
        if (numAce==4||numJack==4||numQueen==4||numKing==4||num9==4||num8==4||num7==4||num6==4||num5==4||num4==4||num3==4||num2==4){
      //System.out.println("Four-of-a-kind");
      four = four+1;}
     else if (numAce==3||numJack==3||numQueen==3||numKing==3||num9==3||num8==3||num7==3||num6==3||num5==3||num4==3||num3==3||num2==3){
     // System.out.println("Three-of-a-kind");
      three = three+1;}
     else if (numAce==2||numJack==2||numQueen==2||numKing==2||num9==2||num8==2||num7==2||num6==2||num5==2||num4==2||num3==2||num2==2){
      //System.out.println("One Pair");
      one = one +1;}
     else{
      // System.out.println("none");
     }
             
        numRun = numRun+1;
     }
 
    DecimalFormat df=new DecimalFormat("0.000");
    System.out.print("The probability of Four-of-a-kind: ");
    System.out.println(df.format((float)four/numHand));
    System.out.print("The probability of Four-of-a-kind: ");
    System.out.println(df.format((float)three/numHand));  
    System.out.print("The probability of One-pair: ");
    System.out.println(df.format((float)one/numHand));
  }
}