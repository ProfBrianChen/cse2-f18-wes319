//Weiyao Shi
//wes319
//lab06
//display pyramids
//pattern a


import java.util.Scanner;
public class PatternA{     
  public static void main(String[] a) {         
    Scanner sc = new Scanner(System.in);//define scanner
     System.out.println("Type in an integer between 1 and 10");
     int input = sc.nextInt();//number of rows
     int i,j,num; 
     if((int)input>=1&&(int)input<=10){//outer loop for number of rows
  
         
        for(i = 0; i < input; i++) 
        { 
             
            num=1; //starting number is 1
  
           
            for(j = 0; j <= i; j++) //number of columns
            { 
             
                System.out.print(num+" "); //print number
  
                //incrementing value of num 
                num++; //increase value
            }
              
            System.out.println(); 
        }
     }
     }
}